package com.bluespurs.starterkit.controller;

import com.bluespurs.starterkit.UnitTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ProductControllerUnitTest extends UnitTest {
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        super.setUp();
        mockMvc = MockMvcBuilders.standaloneSetup(new ProductController()).build();
    }

    /**
     * Test the cheapest product search API, ensures it returns the proper response and data structure.
     *
     * @see ProductController#findCheapestProduct()
     */
    @Test
    public void testProductSearch() throws Exception {
        mockMvc.perform(get("/product/search/?name=ipad")
        	.accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.price").exists())
            .andExpect(jsonPath("$.name").exists())
            .andExpect(jsonPath("$.currency").exists())
            .andExpect(jsonPath("$.location").exists());
    }
}
