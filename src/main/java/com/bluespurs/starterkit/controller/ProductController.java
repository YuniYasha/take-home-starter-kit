package com.bluespurs.starterkit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bluespurs.starterkit.service.product.Product;
import com.bluespurs.starterkit.service.product.ProductService;

@RequestMapping("/product")
@RestController
public class ProductController {
    private static final Logger log = LoggerFactory.getLogger(HelloWorldController.class);

    /**
     * Finds the lowest price product matching a name query string
     * The method is mapped to "/product/search" as a GET request.
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Product findCheapestProduct(@RequestParam("name") String name) {
    	Product product = ProductService.findCheapestProduct(name);
    	
        return product;
    }
}
