package com.bluespurs.starterkit.service.product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class WalmartGateway implements ProductGateway {
	private String APIKEY = "rm25tyum3p9jm9x9x7zxshfa";
	public static final Logger log = LoggerFactory.getLogger(WalmartGateway.class);
	
	/*
	 * Calls the Walmart Product Search API and returns a list of Product objects
	 * @see com.bluespurs.starterkit.service.product.ProductGateway#search(java.lang.String)
	 */
	@Override
	public ArrayList<Product> search(String name) {
		ArrayList<Product> products = new ArrayList<Product>();
		
		try {
			URL url = new URL("http://api.walmartlabs.com/v1/search?apiKey=" + APIKEY + "&query=" + name);
			
			URLConnection connection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String response;
			String fullResponse = "";
			
			while ((response = in.readLine()) != null) {
				fullResponse += response;
			}
			in.close();

			//Ideally instead of having Response Classes and Classes for each Location, I'd set up a single
			//Product class and have a custom serializer and/or builder to build the Product from the JSON data.
			Gson gson = new Gson();
			WalmartResponse responseJSON = gson.fromJson(fullResponse,  WalmartResponse.class);
			for (int i = 0; i < responseJSON.items.length; i++) {
				products.add(responseJSON.items[i]);	
			}
			
		} catch (MalformedURLException error) {
			log.error(error.toString());
		} catch (IOException error) {
			log.error(error.toString());
		}
		
		return products;
	}
}
