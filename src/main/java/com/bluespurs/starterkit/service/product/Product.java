package com.bluespurs.starterkit.service.product;

/*
 * Interface class for Products.
 */
public interface Product {
	public String getName();
	public double getPrice();
	public String getCurrency();
	public String getLocation();
}
