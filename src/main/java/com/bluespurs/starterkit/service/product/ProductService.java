package com.bluespurs.starterkit.service.product;
import com.bluespurs.starterkit.service.product.Product;
import com.bluespurs.starterkit.service.product.ProductGateway;
import com.bluespurs.starterkit.service.product.WalmartGateway;

import java.util.*;
/*
 * Facade service class that calls on several potential gateways to return things like an aggregated list
 */
public final class ProductService {
	private static List<ProductGateway> GATEWAYS = Arrays.asList(new WalmartGateway(), new BestBuyGateway());
    
	/*
	 * Searches products on all gateways by name
	 */
	public static ArrayList<Product> searchProducts(String query) {
		ArrayList<Product> products = new ArrayList<Product>();
		//Loop through the Gateways
		for (int i=0; i < GATEWAYS.size(); i++) {
			ProductGateway gateway = GATEWAYS.get(i);
			ArrayList<Product> _products = gateway.search(query);
			//Loop through the Products in this Gateway
			for (int i2 = 0; i2 < _products.size(); i2++) {
				//Add products to the global products array
				products.add(_products.get(i2));
			}
		}
		
		return products;
	}
	
	/*
	 * Searches all gateways for the cheapest product matching query
	 */
	public static Product findCheapestProduct(String query) {
		ArrayList<Product> products = searchProducts(query);
		Product cheapestProduct = null;
		
		//Loop through all the products
		for (int i=0; i < products.size(); i++) {
			Product product = products.get(i);

			if (cheapestProduct == null || product.getPrice() < cheapestProduct.getPrice()) {
				//Either there is no cheapest product yet or this one has a lower price, update cheapest product
				cheapestProduct = product;
			}
		}
		
		return cheapestProduct;
	}
}
