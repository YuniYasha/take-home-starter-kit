package com.bluespurs.starterkit.service.product;

import java.util.*;

/*
 * Interface for Product Gateways (Ex. Walmart, Bestbuy)
 */
public interface ProductGateway {
	public ArrayList<Product> search (String query);
}
