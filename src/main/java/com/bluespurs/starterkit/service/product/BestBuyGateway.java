package com.bluespurs.starterkit.service.product;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class BestBuyGateway implements ProductGateway {
	private String APIKEY = "pfe9fpy68yg28hvvma49sc89";
	public static final Logger log = LoggerFactory.getLogger(BestBuyGateway.class);
	/*
	 * Calls the Best Buy Product Search API and returns a list of Product objects
	 * @see com.bluespurs.starterkit.service.product.ProductGateway#search(java.lang.String)
	 */
	@Override
	public ArrayList<Product> search(String query) {
		ArrayList<Product> products = new ArrayList<Product>();

		try {
			URL url = new URL("https://api.bestbuy.com/v1/products(search=" + query + ")?format=json&show=sku,name,salePrice&apiKey=" + APIKEY);
			URLConnection connection = url.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));
			String response;
			String fullResponse = "";
			while ((response = in.readLine()) != null) {
				fullResponse += response;
			}
			in.close();
			
			//Ideally instead of having Response Classes and Classes for each Location, I'd set up a single
			//concrete Product class and have a custom serializer and/or builder to build the Product from the JSON data.
			Gson gson = new Gson();
			BestBuyResponse responseJSON = gson.fromJson(fullResponse,  BestBuyResponse.class);
			for (int i = 0; i < responseJSON.products.length; i++) {
				products.add(responseJSON.products[i]);	
			}
			
		} catch (MalformedURLException error) {
			//Handle Error
			log.error(error.toString());
		} catch (IOException error) {
			//Handle Error
			log.error(error.toString());
		}
	
		
		return products;
	}

}
