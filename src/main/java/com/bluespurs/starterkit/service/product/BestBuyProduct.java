package com.bluespurs.starterkit.service.product;

import com.google.gson.annotations.SerializedName;

/*
 * Product class representing a product from BestBuy
 */
public class BestBuyProduct implements Product {
	private String name;
	@SerializedName("salePrice")
	private double price;
	private String currency = "USD";
	private String location = "Best Buy";
	
	public String getName() {
		return this.name;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public String getCurrency() {
		return this.currency;
	}
	
	public String getLocation() {
		return this.location;
	}
}
