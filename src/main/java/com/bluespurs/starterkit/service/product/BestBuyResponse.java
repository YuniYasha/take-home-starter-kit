package com.bluespurs.starterkit.service.product;

/*
 * Class representing a API response from BestBuy.
 */
public class BestBuyResponse {
	public BestBuyProduct[] products;
}
